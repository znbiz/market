# README #

# Необходимо установить! Ubuntu
* Python 2.7* https://www.python.org/

```bash
sudo apt-get install python-pip  python-dev build-essential
pip install --upgrade pip

# Устанавливаем утилиту virtualenv для создания "песочницы" нашего проеткта
pip install virtualenv

# В папке с проектом
virtualenv venv

# Устанавливаем библиотеки
source venv/bin/activate
pip install -r server/requirements.txt
```

* NodeJS

```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

npm install -g gulp
npm install
```



# Необходимо установить! Windows
* Python 2.7* https://www.python.org/

```bash
# Устанавливаем утилиту virtualenv для создания "песочницы" нашего проеткта
pip install virtualenv

# В папке с проектом
virtualenv venv

# Устанавливаем библиотеки
venv\Scripts\pip install -r server/requirements.txt
```

* NodeJS https://nodejs.org/en/ и выполнить команды

```bash
npm install -g gulp
npm install
```

# Запуск
```bash
# запуск gulp и python
npm start # через hamachi
npm run start-local # в локальной сети
```


# Про фронтенд
Собирается все gulp'ом в папку `dist/`. Флаг `--production` собирает в `dist-production`.

## Таски:
* `gulp vendor` - собирает файлы библиотек из `src/vendor`
* `gulp css` - Собирает Stylus из папки `src/css/*.styl`.
* `gulp css --production` - то же самое + минификация
* `gulp copy-glyphicons` - шрифт для бутстрапа из npm
* `gulp img` - сжимает картинки из `src/img`, 10 шакалов из 10
* `gulp js` - собирает js из `src/js/*.js` c помощью `rollup`.
* `gulp js --production` - то же самое + минификация
* `gulp fonts` - собирает шрифты из Google Fonts

## Опции сервера
```shell
python server/server.py --help                                                                                                                                                                 0 ms  master 
usage: server.py [-h] [--db_host DB_HOST] [--db_user DB_USER]
                 [--db_pass DB_PASS] [--db_name DB_NAME]
                 [--postgres_driver POSTGRES_DRIVER] [--debug DEBUG]

optional arguments:
  -h, --help            show this help message and exit
  --db_host DB_HOST     PostgreSQL address, <ip>:<port>
  --db_user DB_USER     PostgreSQL username
  --db_pass DB_PASS     PostgreSQL password
  --db_name DB_NAME     PostgreSQL db name
  --postgres_driver POSTGRES_DRIVER
                        PostgreSQL python driver, psycopg2 (fast, c++), pg8000
                        (slower, python)
  --debug DEBUG         toggle debug mode
```