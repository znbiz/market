from application.extensions import db
from sqlalchemy.dialects.postgresql import JSON
from .security import User


class Messages(db.Model):
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    id_sender = db.Column(db.Integer, db.ForeignKey(
        User.__tablename__ + '.id'), nullable=False)
    user_sender = db.relationship(
        User, foreign_keys=[id_sender])
    date = db.Column(db.Date, nullable=False)
    status = db.Column(db.Integer, nullable=False)
    id_recipient = db.Column(db.Integer, db.ForeignKey(
        User.__tablename__ + '.id'), nullable=False)
    user_recipient = db.relationship(
        User, foreign_keys=[id_recipient])
    topic = db.Column(db.Text)
    time = db.Column(db.Time)

    def __init__(self, id_sender, date, status, id_recipient, topic, time):
        self.id_sender = id_sender
        self.date = date
        self.status = status
        self.id_recipient = id_recipient
        self.topic = topic
        self.time = time


class Message_text(db.Model):
    __tablename__ = 'message_text'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    body_message = db.Column(db.Text)
    file = db.Column(JSON)
