import json

def json_dump_compact(obj):
    return json.dumps(
        obj,
        separators=(',', ':'),
        ensure_ascii=False
    )

