# -*- coding: utf-8 -*-

from application import app, user_datastore
from extensions import db
from models.security import User, Role, Notification
from flask_security import login_required, roles_required, current_user, roles_accepted
from flask import render_template, request, abort, send_from_directory, redirect
from flask_security.utils import encrypt_password
from models.messages import Messages, Message_text
from .utils import json_dump_compact
from werkzeug.utils import secure_filename
import os.path
import math
import datetime
import json
from functools import wraps

# Функция отправляем в БД ошибки
# def error_in_db( msg, id_user ):

def login_required( func ):
    """Decorator which specifies that a user must have been logged in via Kerberos"""
    @wraps( func )
    def wrapper( *args, **kwargs ):
        if current_user.is_authenticated:
            return func( *args, **kwargs )
        return redirect( '/login' )
    return wrapper


@app.route("/", methods=['GET'])
def hello():
    login = False
    if current_user.is_authenticated:
        login = current_user.email

    return render_template(
        "index.pug",
        page_title="Главная",
        login=login
    )




# ______________________________________________ Админка ___________________________________________ #
@app.route("/admin", methods=['GET'])
@login_required
@roles_required('admin')
def admin():
    users_query = db.session.query(User).all()

    users_list = []
    for user in users_query:
        # For every user get correct name of his organization
        name = ''

        i = 0
        for role in user.roles:
            i += 1
        if i > 0:
            if user.roles[0].name == 'admin':
                name = 'Администратор'
            elif user.roles[0].name == 'departament':
                name = 'Департамент министерства промышленности и торговли'

        users_list.append({
            'login': user.email,
            'name': name
        })

    return render_template(
        "admin.pug",
        page_title="Администрирование",
        user_list=json_dump_compact(users_list),
        login=current_user.email
    )


@app.route("/admin/add-user", methods=['POST'])
@login_required
@roles_required('admin')
def add_user():
    login_user = request.form.get('email')
    new_password = request.form.get('password')
    new_password_confirm = request.form.get('password_confirm')
    role = request.form.get('role')
    roles_query = db.session.query(Role).all()  # Make list of roles to check if user role is valid
    roles = [r.name for r in roles_query]
    user_to_change = db.session.query(User) \
        .filter(User.email == login_user) \
        .one_or_none()

    if not user_to_change:
        if new_password == new_password_confirm:
            if role in roles:
                # If everything is ok, first create user and then add role to it
                try:
                    user_datastore.create_user( 
                        email=login_user,
                        password=encrypt_password(new_password)
                    )
                    db.session.commit()
                except:
                    return json.dumps({'success': False, 'error': 'Ошибка'})
                user_datastore.remove_role_from_user(login_user, 'Organization')
                user_datastore.add_role_to_user(login_user, role)
                db.session.commit()
                return json.dumps({'success': True})
            else:
                return json.dumps({'success': False, 'error': 'Плохие данные по правам доступа'})
        else:
            return json.dumps({'success': False, 'error': 'Пароли не совпадают'})
    else:
        return json.dumps({'success': False, 'error': 'Пользователь с таким именем уже существует'})


@app.route("/admin/delete-user", methods=['POST'])
@login_required
@roles_required('admin')
def user_delete():
    login_user = request.form.get('login-delete')
    delete_user = db.session.query(User) \
        .filter(User.email == login_user) \
        .one_or_none()

    if delete_user:
        db.session.delete(delete_user)
        db.session.commit()
        return json.dumps({'success': True})
    else:
        return json.dumps({'success': False, 'error': 'Пользователь не найден'})


@app.route("/admin/changepsw", methods=['POST'])
@login_required
@roles_required('admin')
def changepsw():
    login_user = request.form.get('login')
    new_password = request.form.get('new_password')
    new_password_confirm = request.form.get('new_password_confirm')
    user_to_change = db.session.query(User) \
        .filter(User.email == login_user) \
        .one_or_none()

    if user_to_change:
        if new_password == new_password_confirm:
            user_to_change.password = encrypt_password(new_password)
            db.session.commit()
            return json.dumps({'success': True})
        else:
            return json.dumps({'success': False, 'error': 'Пароли не совпадают'})
    else:
        return json.dumps({'success': False, 'error': 'Пользователь не найден'})

# ______________________________________________________________________________________________ #


@app.route("/messages", methods=['GET', 'POST'])
@login_required
def messages():
    user_role = current_user.roles[0].name
    user_id = current_user.id

    if user_role == 'admin' or user_role == 'departament':
        if request.method == 'GET':
            org_list = db.session.query(User) \
                .all()

            messages_list = db.session.query(Messages, User) \
                .filter(Messages.id_recipient == user_id) \
                .join(User, User.id == Messages.id_sender) \
                .all()

            contacts = []

            for contact in org_list:
                if (user_role != 'departament') and (contact.roles[0].name == 'departament'):
                    if user_id != contact.id:
                        contacts.append({
                            'id': contact.id_information_organization,
                            'id_name': contact.id,
                            'name': 'Департамент'
                        })
                else:
                    if (user_role != 'admin') and (user_id != contact.id):
                        contacts.append({
                            'id': contact.id_information_organization,
                            'id_name': contact.id,
                            'name': 'Администратор'
                        })

            messages = []
            for message in messages_list:
                if message.User.roles[0].name == 'departament':
                    messages.append({
                        'id_sender': message.Messages.id_sender,
                        'name_sender': 'Минпромторг',
                        'topic': message.Messages.topic,
                        'time': str(message.Messages.time),
                        'date': str(message.Messages.date),
                        'status': message.Messages.status})
                elif message.User.roles[0].name == 'admin':
                    messages.append({
                        'id_sender': message.Messages.id_sender,
                        'name_sender': 'Администратор',
                        'topic': message.Messages.topic,
                        'time': str(message.Messages.time),
                        'date': str(message.Messages.date),
                        'status': message.Messages.status})

        if request.method == 'POST':
            now = datetime.datetime.now()
            date_time = str(now.time())
            new_message = Messages(user_id, datetime.date.today(), 0, request.form.get('id_recipient'),
                                   request.form.get('topic'), date_time)
            db.session.add(new_message)
            db.session.commit()
            new_message_text = Message_text(id=new_message.id, body_message=request.form.get('body_message'))
            db.session.add(new_message_text)
            db.session.commit()
    else:
        if request.method == 'GET':
            org_list = db.session.query(User) \
                .all()

            messages_list = db.session.query(Messages, User) \
                .filter(Messages.id_recipient == user_id) \
                .join(User, User.id == Messages.id_sender) \
                .all()

            contacts = []

            for contact in org_list:
                if contact.roles[0].name == 'departament':
                    if user_id != contact.id:
                        contacts.append({
                            'id': contact.id_information_organization,
                            'id_name': contact.id,
                            'name': 'Минпромторг'
                        })
                else:
                    if contact.roles[0].name == 'admin':
                        contacts.append({
                            'id': contact.id_information_organization,
                            'id_name': contact.id,
                            'name': 'Администратор'
                        })

            messages = []
            for message in messages_list:
                if message.User.roles[0].name == 'departament':
                    messages.append({
                        'id_sender': message.Messages.id_sender,
                        'name_sender': 'Минпромторг',
                        'topic': message.Messages.topic,
                        'time': str(message.Messages.time),
                        'date': str(message.Messages.date),
                        'status': message.Messages.status})
                elif message.User.roles[0].name == 'admin':
                    messages.append({
                        'id_sender': message.Messages.id_sender,
                        'name_sender': 'Администратор',
                        'topic': message.Messages.topic,
                        'time': str(message.Messages.time),
                        'date': str(message.Messages.date),
                        'status': message.Messages.status})

        if request.method == 'POST':
            now = datetime.datetime.now()
            date_time = str(now.time())
            new_message = Messages(user_id, datetime.date.today(), 0, request.form.get('id_recipient'),
                                   request.form.get('topic'), date_time)
            db.session.add(new_message)
            db.session.commit()
            new_message_text = Message_text(id=new_message.id, body_message=request.form.get('body_message'))
            db.session.add(new_message_text)
            db.session.commit()

    return render_template(
        '/user/messages.pug',
        page_title='Сообщения',
        contacts=json_dump_compact(contacts),
        messages=json_dump_compact(messages),
        login=current_user.email
    )


@app.route("/messages/id", methods=['POST'])
@login_required
def messages_history():
    user_id = current_user.id
    id_history = request.form.get('id')

    messages_list_history_query = db.session.query(Messages, Message_text, User) \
        .filter(((Messages.id_recipient == user_id) & (Messages.id_sender == id_history)) |
                ((Messages.id_recipient == id_history) & (Messages.id_sender == user_id))) \
        .join(User, User.id == id_history) \
        .join(Message_text, Message_text.id == Messages.id) \
        .all()

    messages_history = []
    for message in messages_list_history_query:
        if message.Messages.user_sender.roles[0].name == 'admin':
            sender_name = 'Администратор'
            sender_id = None
            sender_user_id = message.Messages.user_sender.id
        elif message.Messages.user_sender.roles[0].name == 'departament':
            sender_name = 'Минпромторг'
            sender_id = None
            sender_user_id = message.Messages.user_sender.id

        if message.Messages.user_recipient.roles[0].name == 'admin':
            recipient_name = 'Администратор'
            recipient_id = None
            recipient_user_id = message.Messages.user_recipient.id
        elif message.Messages.user_recipient.roles[0].name == 'departament':
            recipient_name = 'Минпромторг'
            recipient_id = None
            recipient_user_id = message.Messages.user_recipient.id

        messages_history.append({
            'data': str(message.Messages.date),
            'time': str(message.Messages.time),
            'from': {'name': sender_name,
                     'id_okpo': sender_id,
                     'sender_user_id': sender_user_id},
            'to': {'name': recipient_name,
                   'id_okpo': recipient_id,
                   'recipient_user_id': recipient_user_id},
            'text': message.Message_text.body_message,
            'topic': message.Messages.topic,
            'file': None
        })
    data_result = {"id_history": id_history,
                   'data': messages_history}

    return json_dump_compact(data_result)