from application import app

##########################################################################

def main():
    # try:
    #     from gevent.wsgi import WSGIServer
    #     http_server = WSGIServer(('', 6000), app)
    #     http_server.serve_forever()
    #     return
    # except ImportError:
    #     print "GEvent not found. " \
    #           "Failed to start gevent based server. " \
    #           "Falling back to tornado."

    # try:
    #     from tornado.wsgi import WSGIContainer
    #     from tornado.httpserver import HTTPServer
    #     from tornado.ioloop import IOLoop

    #     http_server = HTTPServer(WSGIContainer(app))
    #     http_server.listen(4500)
    #     IOLoop.instance().start()
    #     return
    # except ImportError:
    #     print "Tornado not found. " \
    #           "Failed to start tornado based server. " \
    #           "Falling back to flask run()."

    app.run(host='0.0.0.0', port=80, threaded=True)

##########################################################################

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == "__main__":
    main()
