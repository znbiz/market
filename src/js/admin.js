$(document).ready(function () {


    function checkPassword(pswd) {
        var password = pswd;
        var s_letters = "qwertyuiopasdfghjklzxcvbnm"; // Буквы в нижнем регистре
        var b_letters = "QWERTYUIOPLKJHGFDSAZXCVBNM"; // Буквы в верхнем регистре
        var digits = "0123456789"; // Цифры
        var specials = "!@#$%^&*()_-+=\|/.,:;[]{}"; // Спецсимволы
        var is_s = false; // Есть ли в пароле буквы в нижнем регистре
        var is_b = false; // Есть ли в пароле буквы в верхнем регистре
        var is_d = false; // Есть ли в пароле цифры
        var is_sp = false; // Есть ли в пароле спецсимволы
        for (var i = 0; i < password.length; i++) {
          /* Проверяем каждый символ пароля на принадлежность к тому или иному типу */
          if (!is_s && s_letters.indexOf(password[i]) != -1) is_s = true;
          else if (!is_b && b_letters.indexOf(password[i]) != -1) is_b = true;
          else if (!is_d && digits.indexOf(password[i]) != -1) is_d = true;
          else if (!is_sp && specials.indexOf(password[i]) != -1) is_sp = true;
        }
        var rating = 0;
        var text = "";
        if (is_s) rating++; // Если в пароле есть символы в нижнем регистре, то увеличиваем рейтинг сложности
        if (is_b) rating++; // Если в пароле есть символы в верхнем регистре, то увеличиваем рейтинг сложности
        if (is_d) rating++; // Если в пароле есть цифры, то увеличиваем рейтинг сложности
        if (is_sp) rating++; // Если в пароле есть спецсимволы, то увеличиваем рейтинг сложности
        /* Далее идёт анализ длины пароля и полученного рейтинга, и на основании этого готовится текстовое описание сложности пароля */
        if (password.length < 6 && rating < 3) text = "Простой";
        else if (password.length < 6 && rating >= 3) text = "Средний";
        else if (password.length >= 8 && rating < 3) text = "Средний";
        else if (password.length >= 8 && rating >= 3) text = "Сложный";
        else if (password.length >= 6 && rating == 1) text = "Простой";
        else if (password.length >= 6 && rating > 1 && rating < 4) text = "Средний";
        else if (password.length >= 6 && rating == 4) text = "Сложный";
        alert(text); // Выводим итоговую сложность пароля
        return false; // Форму не отправляем
    }

    $('#role-multiselect').multiselect({
        enableCaseInsensitiveFiltering: true
    });
    $('#org-multiselect').multiselect({
        enableCaseInsensitiveFiltering: true
    });
    

    function transformation(data) {
        for (var i =0; i < data.length; i++) {
            data[i]['new_password'] = '<a href="#new-password" onclick="$(\'#login\').val(\'' +
                                data[i].login + '\');$(\'#new-pswd\').val(\'\');$(\'#repeat-new-pswd\').val(\'\');' +
                                '$(\'#new-pswd\').css({\'border\': \'\'});' +
                                '$(\'#repeat-new-pswd\').css({\'border\': \'\'});"' +
                                'class="glyphicon glyphicon-edit" style="font-size:22px;" data-toggle="modal"></a>';

            data[i]['delete'] = '<a href="#delete" onclick="$(\'#login-del\').html(\'' +
                                data[i].login + '?\'); $(\'#login-delete-none\').val(\'' +
                                data[i].login + '\');" class="glyphicon glyphicon-remove" style="font-size:22px; color: red" data-toggle="modal"></a>';
        }
        return data;
    }

    user_list = transformation(user_list);
    $('#user-table').DataTable({
        data: user_list,
        columns: [
            {
                title: 'Пользователь',
                data: 'login'
            },
            {
                data: 'new_password',
                "orderable": false
            },
            {
                data: 'delete',
                "orderable": false
            }
        ]
    });

    $('#add-user-form').submit(()=>{
        if (/['"\/#$%^&*!?;]/.test($('#email').val())) {
            alert('Логин не должен содержать символы:\'\"\\/#$%^&*!?;')
            $('#email').css({'border': '2px solid #f00'});
            return false;
        } else {
            if ($('#pswd').val() != $('#repeat-pswd').val()) {
                alert('Пароли не совпадают');
                $('#pswd').css({'border': '2px solid #f00'});
                $('#repeat-pswd').css({'border': '2px solid #f00'});
            } else {
                var data = $('#add-user-form').serialize();
                $.ajax({
                    url:'/admin/add-user',
                    type:'post',
                    data:data,
                    dataType: 'JSON',
                    success: function(data) {
                        if(data.success) {
                            $(location).attr("href",'/admin');
                        } else {
                            alert(data.error);
                        }
                    }
                });
            }
        }
        return false;
    });

    $('#new-pswd-form').submit(()=>{
        if ($('#new-pswd').val() != $('#repeat-new-pswd').val()) {
            alert('Пароли не совпадают');
             $('#new-pswd').css({'border': '2px solid #f00'});
             $('#repeat-new-pswd').css({'border': '2px solid #f00'});
        } else {
            var data = $('#new-pswd-form').serialize();
            data+= '&login=' + $('#login').val()
            $.ajax({
                url:'/admin/changepsw',
                type:'post',
                data:data,
                dataType: 'JSON',
                success: function(data) {
                    if(data.success) {
                        $(location).attr("href",'/admin');
                    } else {
                        alert(data.error);
                    }
                }
            });
        }
        return false;
    });

    $('#delete-form').submit(()=>{
        var data = $('#delete-form').serialize();
        $.ajax({
            url:'/admin/delete-user',
            type:'post',
            data:data,
            dataType: 'JSON',
            success: function(data) {
                if(data.success) {
                    $(location).attr("href",'/admin');
                } else {
                    alert(data.error);
                }
            }
        });
        return false;
    });
});
