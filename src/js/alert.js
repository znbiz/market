$(()=>{
	function send(element, id) {
        $(document).ready(function () {
    		$(element).bind('click', ()=>{
    			$.ajax({
    		        type: 'POST',
    		        url: '/notifications',
    		        dataType: 'JSON',
    		        data: {'id':id}
    		    });
    		});
            $(element).find('.toast-close-button').bind('click', ()=>{
                $.ajax({
                    type: 'POST',
                    url: '/notifications',
                    dataType: 'JSON',
                    data: {'id':id}
                });
            });
        });
	}

	$.ajax({
        type: 'GET',
        url: '/notifications',
        dataType: 'JSON',
        success: function (data) {
        	for(var i=0; i<data.length;i++)
        	{
        		var alert = data[i];
        		if(alert.message.status == 1) {
                    send(Toastr["success"](alert.message.message, "Уведомление"), data[i].id);
        		} else if(alert.message.status == -1) {
        			send(Toastr["error"](alert.message.message, "Уведомление"), data[i].id);
        		}
        	}
        }
    });
});