$(document).ready(function () {
    function to_send_post(name_id) {
        $(document).ready(function () {
            $('#button-to-send').bind('click', function () {
                $.ajax({
                    type: 'POST',
                    url: '',
                    data: {
                        id_recipient: name_id,
                        topic: $('#topic-message').val(),
                        body_message: $('#body-message').val()
                    },
                    dataType: 'JSON',
                    success: function (data, textStatus, jqXHR) {
                    }
                });
                list_message_load(name_id);
            });
        });
    }

    function list_message_load(id) {
        $.ajax({
            type: 'POST',
            url: '/messages/id',
            data: {id: id},
            dataType: 'JSON',
            success: function (data, textStatus, jqXHR) {
                draw_correspondence(data);
            }
        });
    }

    function draw_correspondence(data) {
        messages = data.data;
        messages.sort(function (a, b) {
            var mas_date_a = [];
            var mas_date_b = [];
            mas_date_a.push(a.data.split('-'));
            mas_date_a.push(a.time.split(':'));
            mas_date_b.push(b.data.split('-'));
            mas_date_b.push(b.time.split(':'));
            for (var i = 0; i < mas_date_a.length; i++) {
                if (mas_date_b[i] > mas_date_a[i]) {
                    return true;
                }
            }
        });
        var str = '';
        str += '<div class="col-xs-8">';
        str += '<div class="col-xs-12">';
        str += '<label class="control-label">Тема сообщения</label><input id="topic-message" class="form-control"/>';
        str += '</div><div class="col-xs-12">';
        str += '<label class="control-label">Текст сообщения</label><textarea id="body-message" class="form-control"/>';
        str += '</div>';
        str += '</div><br>';
        str += '<button id="button-to-send" class="btn btn-primary" type="submit">Отправить</button>';
        messages.forEach(function (entry) {
            str += '<tbody>';
            str += '<tr>';
            str += '<td>' + entry.data + '  ' + entry.time +'</td>';
            str += '<td><blockquote class="blockquote-reverse">';
            str += '<p>' + entry.text + '</p>';
            str += '<footer>' + entry.from.name + '</footer>';
            str += '</blockquote></td>';
            str += '</tr>';
            str += '</tbody>';
            entry['id_name'] = entry.id_sender;
        });
        $('#message-table').html(str);
        to_send_post(data.id_history);

    }

    function correspondence_request(data, str_type) {
        $(document).ready(function () {
            data.forEach(function (entry) {
                $('#name-'+str_type+entry.id_name).bind('click', function () {
                    list_message_load(entry.id_name);
                });
            });
        });
    }

    /*
     Список сообщений
     */
    function transformation_list_messages(data) {
        data.forEach(function (entry) {
            entry.name = '<div id="name-'+entry.id_name+'">'+entry.name+'</div>';
        });
        return data;
    }

    function draw(data) {
        var str = '<thead><tr><td>От кого</td><td>Дата</td><td>Тема</td></tr></thead>';
        data.forEach(function (entry) {
            str += '<tbody>';
            str += '<tr id="name-message'+entry.id_sender+'">';
            str += '<td>' + entry.name_sender + '</td>';
            str += '<td>' + entry.date + ' ' + entry.time+'</td>';
            str += '<td>' + entry.topic + '</td>';
            str += '</tr>';
            str += '</tbody>';
            entry['id_name'] = entry.id_sender;
        });
        $('#message-table').html(str);
        correspondence_request(data, 'message');
    }
    draw(messages);


    /*
     Контакты
     */
    function transformation_list_contact(data) {
        data.forEach(function (entry) {
            entry.name = '<div id="name-contact'+entry.id_name+'">'+entry.name+'</div>';
        });
        return data;
    }

    contacts = transformation_list_contact(contacts);
    $('#contacts-table').DataTable({
        data: contacts,
        columns: [{title: 'Предприятие',
                data: 'name'
                }],
        paging: false,
        ordering: false,
        searching: false,
        info: false
    });
    correspondence_request(contacts, 'contact');
});
