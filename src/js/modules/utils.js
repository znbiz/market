export default class Utils {
    static clone(obj) {
        /**
         * Быстрое глубокое клоирование
         * @type {Object}
         */
        var cloner = {
            _clone: function _clone(obj) {
                if (obj instanceof Array) {
                    var out = [];
                    for (var i = 0, len = obj.length; i < len; i++) {
                        var value = obj[i];
                        out[i] = (value !== null && typeof value === "object") ? _clone(value) : value;
                    }
                } else {
                    var out = {};
                    for (var key in obj) {
                        if (obj.hasOwnProperty(key)) {
                            var value = obj[key];
                            out[key] = (value !== null && typeof value === "object") ? _clone(value) : value;
                        }
                    }
                }
                return out;
            },

            clone: function(it) {
                return this._clone({
                it: it
                }).it;
            }
        };

        return cloner.clone(obj);
    }

    /**
     * 
     */
    static sort_string(str1, str2) {
        var rx = /([^\d]+|\d+)/ig;
        var str1split = str1.match( rx );
        var str2split = str2.match( rx );
        for(var i=0, l=Math.min(str1split.length, str2split.length); i < l; i++) {
            var s1 = str1split[i], 
            s2 = str2split[i];
            if (s1 === s2) continue;
            if (isNaN(+s1) || isNaN(+s2))
                return s1 > s2 ? 1 : -1;
            else
                return +s1 - s2;    
        }
        return 0;
    }

    // возвращает cookie с именем name, если есть, если нет, то undefined
    static  getCookie(name) {
          var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
          ));
          return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    static setCookie(name, value, options) {
      options = options || {};

      var expires = options.expires;

      if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
      }
      if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
      }

      value = encodeURIComponent(value);

      var updatedCookie = name + "=" + value;

      for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
          updatedCookie += "=" + propValue;
        }
      }

      document.cookie = updatedCookie;
    }

    /**
     * Определяет систему и выдаёт SUM для Linux и СУММ для Windows. Это нужно для печати в экселе суммы сумирования
     * @return {string} 'SUM' или 'СУММ'
     */
    static lin_win_sum() {
        // Определяем "семейство" операционной системы:
        var os = 0;
        if (navigator.userAgent.indexOf ('Windows') != -1) os = 1;
        if (navigator.userAgent.indexOf ('Linux')!= -1) os = 2;
        if (navigator.userAgent.indexOf ('Mac')!= -1) os = 3;
        if (navigator.userAgent.indexOf ('FreeBSD')!= -1) os = 4;
        // alert(navigator.userAgent);
        switch (os) {
           case 1:
              return 'СУММ'
              break;
           case 2:
              return 'SUM'
              break;
           case 3:
              return 'SUM'
              break;
           case 4:
              return 'SUM'
              break;
           default:
              return 'SUM'
              break;
        }
    }

    /**
     *
     * @param {Number} min
     * @param {Number} max
     * @param {Number} [step]
     */
    static range(min, max, step = 1) {
        return Array.from(
            new Array(max - min), (x, i) => i * step + min
        );
    }

    /**
     * Функция отрисовывает в блоке с id "id_draw" значения, которые выбраны в селекте с id 'id_select'. Работает возможность удаления
     * выделеных элементов по средствам нажатия на отрисованный элемент только в случае если включён мултиселект бутстрапа
     * @param  {string} id_draw   - id блока, в котором будут отображены выделеные опции из селекта
     * @param  {string} id_select - id селекта
     */
    static draw_value_select_updata_multiselect(id_draw, id_select) {
        var remove_select = (id_button, name_select, id_select, id_draw) => {
            $('#' + id_button).unbind();
            $('#' + id_button).bind('click', () => {
                $('#'+id_select).next().children('.multiselect-container').find('input[value='+name_select+']').parent().click();
                draw_value(id_select, id_draw);
            });
        };
        var hendler_select = (id_select, id_draw) => {
            $('#'+id_select).bind('change', () => {
                draw_value(id_select, id_draw);
            });
        };
        var draw_value = (id_select, id_draw) => {
            var button = {};
            var val = '';
            var word_len = 0;
            var flag_more = false;
            $('#'+id_select + ' option:selected').each(function(){
                val += '<button id="' + id_select + '-button' + this.value + '" style="white-space: normal!important; text-transform: none!important; font-size: 11px!important;margin: 1px!important;" class="btn btn-default btn-xs">';
                val += this.label;
                val += '<span class="glyphicon glyphicon-remove"></span></button>';
                button[id_select + '-button' + this.value] = this.value;
                if(word_len > 1300 && (!flag_more)) {
                    flag_more = true;
                    val += '<span style="display:none" id="'+id_select+'-button-span">';
                } else {
                    word_len += this.label.length;
                }
            });
            if(flag_more) {
                val += '</span><p class="text-center"><a id="'+id_select+'-button-span-collapse-in"><b>Показать все выбранные элементы...</b></a> \
                        <a id="'+id_select+'-button-span-collapse" style="display:none"><b>Скрыть</b></a></p>';
            }

            $('#'+id_draw).html(val);
            for(var id_button in button) {
                remove_select(id_button, button[id_button], id_select, id_draw);
            }
            if(flag_more) {
                $('#'+id_select+'-button-span-collapse-in').bind('click', ()=> {
                    $('#'+id_select+'-button-span-collapse-in').css('display','none');
                    $('#'+id_select+'-button-span').css('display','block');
                    $('#'+id_select+'-button-span-collapse').css('display','block');
                });
                $('#'+id_select+'-button-span-collapse').bind('click', ()=> {
                    $('#'+id_select+'-button-span-collapse-in').css('display','block');
                    $('#'+id_select+'-button-span').css('display','none');
                    $('#'+id_select+'-button-span-collapse').css('display','none');
                });
            }
        };
    }
}
