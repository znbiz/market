import DataTable from 'datatables.net-bs';
DataTable(window, $);
$.extend(true, $.fn.dataTable.defaults, {
    searching: true,
    language: {
        processing: 'Подождите...',
        search: 'Поиск:',
        lengthMenu: 'Показать _MENU_ записей',
        info: 'Записи с _START_ до _END_ из _TOTAL_ записей',
        infoEmpty: 'Записи с 0 до 0 из 0 записей',
        infoFiltered: '(отфильтровано из _MAX_ записей)',
        infoPostFix: '',
        loadingRecords: 'Загрузка записей...',
        zeroRecords: 'Записи отсутствуют.',
        emptyTable: 'В таблице отсутствуют данные',
        paginate: {
            first: 'Первая',
            previous: 'Предыдущая',
            next: 'Следующая',
            last: 'Последняя'
        },
        aria: {
            sortAscending: ': активировать для сортировки столбца по возрастанию',
            sortDescending: ': активировать для сортировки столбца по убыванию'
        }
    },
    deferRender: true
});

/* Добавляем сортировку чисел с форматированием */
$.extend(true, $.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = a.toString();
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
        return parseFloat( a );
    },
 
    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },
 
    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
});

import buttons from 'datatables-buttons';
buttons(window, $);
import buttons_bootstrap from 'datatables-buttons/js/buttons.bootstrap';
buttons_bootstrap(window, $);
import buttons_print from 'datatables-buttons/js/buttons.print';
buttons_print(window, $);