import './components/numeral-global.js';
const language = {
    delimiters: {
        thousands: ' ',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'тыс.',
        million: 'млн',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function () {
        // not ideal, but since in Russian it can taken on
        // different forms (masculine, feminine, neuter)
        // this is all we can do
        return '.';
    },
    currency: {
        symbol: 'руб.'
    }
};
window.numeral.language('ru', language);
window.numeral.language('ru');


//window.numeral.MONEY_FORMAT = '0,0[.]00 $';
window.numeral.MONEY_FORMAT = '0,0.00 $';
window.numeral.MONEY_FORMAT_NO = '0,0.00';
window.numeral.my_fun = function (number) {
                var n = number.toString();
                var n = n.split('.')[0].split('').reverse().join('');
                var res = '';
                for(var i = 0; i < n.length; i++) {
                    res += (i % 3 == 0 && i != 0? ','+n[i] : n[i]);
                }
                return res.split('').reverse().join('');
            }
window.numeral.my_fun_space = function (number) {
                var n = number.toString();
                var n = n.split('.')[0].split('').reverse().join('');
                var res = '';
                for(var i = 0; i < n.length; i++) {
                    res += (i % 3 == 0 && i != 0? ' '+n[i] : n[i]);
                }
                return res.split('').reverse().join('');
            }
